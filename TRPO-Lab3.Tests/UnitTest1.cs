using NUnit.Framework;
using System;
using Lib3;




namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Test1()
        {
            const double i = 164.15;
            Double S = Class1.GetSq(15, 4, 90);
            Assert.AreEqual(i, S);

        }

        [Test]
        public void Testing()
        {
            Assert.Throws<ArgumentException>(() => Class1.GetSq(-15, 4, -90));
        }





    }  
}